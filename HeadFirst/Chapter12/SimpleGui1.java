import javax.swing.*;

public class SimpleGui1 
{
 public static void main  (String[] args) 
 {
  JFrame frame = new JFrame(); //make a frame and a button
  JButton button = new JButton("click me"); 
  
  frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
  //this makes the program quite when you close the window

  frame.getContentPane().add(button);
  //add the button to the frame's content pane

  frame.setSize(300, 300);
  //give the frame a size in pixels

  frame.setVisible(true);
  //make it visible
 } 
}


