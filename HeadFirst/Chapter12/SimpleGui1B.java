import javax.swing.*; 
import javax.awt.event.*; 
//1) a new import statement for the package that ActionListener and ActionEvent are in

public class SimpleGui1B implements ActionListener 
 { //implement the interface
  JButton button; 
  
  public static void main (String[] args) 
  {
    SimpleGui1B gui = new SimpleGui1b(); 
    gui.go();
  }

  public void go() 
  {
    JFrame frame = new JFrame();
    button = new JButton("click me");
    button.addActionListener(this); 
//2)register your interest with the button,the argument passed MUST be an object from a class that implements ActionListener

    frame.getContentPane().add(button);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(300, 300);
    frame. setVisible(true);
  }

  public void actionPreformed(ActionEvent event)
//3)implement the ActionListener interface's actionPreformed() method, this is the actual event-handling method 
  {
    button.setText("I've been clicked");
  }
}
