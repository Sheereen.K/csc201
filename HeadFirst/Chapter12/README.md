## Making a GUI is easy:

1) Make a frame(a JFrame)
```sh
JFrame frame = new JFrame();
```
2) Make a widget (button, text field, etc.)
```sh
JButton button = new JButton("click me");
```
3) Add the widget to the frame
```sh
frame.getContentPane() .add(button);
```
 - Do not add things to the frame directly, add through a window pane

4) Display it (give it a size and make it visible)
```sh
frame.setSize(300, 300);
frame.setVisible(true);
```
## Getting a user event:

- The process of getting and handling a user event is called event-handling
- If the user clicks a button that is an event

### If you care about the button's events, implement an interface that says, "I'm listening for your events."
- A listener interface is the bridge between the listener and the event source
- When you implement a listener interface, you give the button a way to call you back. **The interface is where the call-back method is declared**

## Getting a button's ActionEvent:
1. Implement the ActionListener interface
2. Register with the button(tell it you want to listen for events)
3. Define the event-handling method(implement the actionPreformed() method from the ActionListener interface)

## Listeners, Sources, and Events
- Listener GETS the event
- Source SENDS the event
- Event object HOLDS DATA about the event

## Getting back to graphics
1. Put widgets on a frame, add buttons, menus, radio buttons, etc.
```sh
frame.getContentPane().add(myButton);2. Draw 2D graphics on a widget, use a graphics object to paint shapes
```sh
graphics.fillOval(70, 70, 100, 100);
```
3. Put a JPEG on a widget, you can put your own images on a widget
```sh
graphics.drawImage(myPic, 10, 10, this);
```
- you can have one class nested inside another
```sh
class MyOuterClass
 {
  class MyInnerClass
   {
     void go()
      {
      }
   }
}
```
### Inner class using an outer class variable:

```sh
class MyOuterClass
{
  private int x;
 class MyInnerClass 
  {
   void go()
    {
     x = 42; //use 'x' as if it were a variable of the inner class
    }
  } //close inner class
} //close outer class
```

## How to make an instance of an inner class:

```sh
class MyOuter
{
 private int x;
 MyInner inner = new MyInner();

 public void doStuff() {
   inner.go()
 }

 class MyInner {
  void go() {
    x = 42
   }
 }
}
```
## Listening for a non-GUI event:
1. Make a series of MIDI messages/events to play random notes on an piano
2. Register a listener for the events
3. Start the sequencer playing
4. Each time the listener's event handler method is called, draw a random rectanlge on the drawing panel, and call repaint

### Things that have to happen for each event:
1. Make a message instance
```sh
ShortMessage first = new ShortMessage();
```
2. Call setMessage() with the instructions
```sh
first.setMessage(192, 1, insturment, 0)
```
3. Make a MidiEvent instance for the message
```sh
MidiEvent noteOn = new MidiEvent(first, 1);
```
4. Add the event to the track
```sh
track.add(notOn);
```

