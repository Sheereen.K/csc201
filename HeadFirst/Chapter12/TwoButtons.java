import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

public class TwoButtons 
 { //the main GUI class doesn't implement ActionListener now
  JFrame frame;
  JLabel label;

  public static void main (String[] args)
  { 
    TwoButtons gui = new TwoButtons ();
    gui.go();
  }

  public void go() 
  {
    frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); 

    JButton labelButton = new JButton("Change Label");
    labelButton.addActionListener(new LabelListener()); //pass a new instance of the appropriate listener class instead of passing (this) 

    JButton colorButton = new JButton("Change Circle");
    colorButton.addActionListener(new ColorListener()); 

    label = new JLabel("I'm a label"); 
    MyDrawPanel drawPanel = new MyDrawPanel(); 

    frame.getContentPane().add(BorderLayout.SOUTH, colorButton);
    frame.getContentPane().add(BorderLayout.CENTER, drawPanel);
    frame.getContentPane().add(BorderLayout.EAST, labelButton);
    frame.getContentPane().add(BorderLayout.WEST, label);

    frame.setSize(300, 300);
    frame.setVisible(true);
   }

   class LabelListener implements ActionListener //first ActionListener
   { 
     public void actionPerformed(ActionEvent event)
     {
	label.setText("Ouch!"); //inner class knows about 'label'
     }
   } //close inner class
   class ColorListener implements ActionListener //second ActionListener
   {
     public void actionPerformed(ActionEvent event)
     {
	frame.repaint(); //the inner class gets to use the 'frame' instance variable, without having an explicit reference to the outer class object.
     }
   } //close inner class
} 
