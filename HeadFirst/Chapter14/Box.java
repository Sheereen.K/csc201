import java.io.*; //serializeable is the java.io pacakge

public class Box implements Serializable //no methods to implement but serializable changes that
 {
    private int width;
    private int height; //these two will be saved

    public void setWidth(int w)
    {
       width = w;
    }

    public void setHeight(int h) 
    {
      height = h;
    }

 public static void main (String[] args) 
   {
     Box myBox = new Box();
     myBox.setWidth(50);
     myBox.setHeight(20);

     try { //
       fileOutputStream fs = new FileOutputStream("foo.ser");
         ObjecttOutputStream os = new ObjectOutputStream(fs);
         os.writeObject(myBox);
         os.close();
       } catch(Exception ex) {
           ex.printStackTrace();
      }
   }
}


