import java.io.*;

class ReadAFile {
   public static void main (String[] args) {
    
     try {
	File myFile = new File("MyText.txt");
	FileReader fileReader = new FileReader(myFile);
//cahin the FileReader to a BufferedReaderfor more efficient reading. 
	BufferedReader reader = new BufferedReader(fileReader);
//make a string variable to hold each line as the line is read
	String line = null; 
	while ((line = reader.readLine()) != null) {
	   System.out.println(line);

	}
	reader.close();
    } catch(Exception ex) {
	ex.printStackTrace();
    }
  }
}

