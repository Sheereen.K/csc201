## Saving Objects
- objects have state and behavior
- Objects can be flattened and inflated

### Capture the Beat
- need to make a Save button

#### If your data will be used by only the Java program that generated it:
1. **Use serialization**
Write a file that holds flattened(serialized) objects.

#### If your data will be used by other programs: 
2. **Write a plain text file**
Write a file, with delimiters that other programs can parse.
 
### Saving State 
1. Option One:
**Write the three serialized character objects to a file.**
Creat a file and write three serialized character objects (Harder to read but much safer for the program incase there are some errors)

2. Option Two: 
**Write a plain text file**
Create a file and write three lies of text one per character, seperating the pieces of state with commas.

### Writing a serialized object to a file:
1. Make a FileOutputStream
```sh 
FileOutputStream fileStream = new FileOutputStream("MyGame.ser");
```
creating a file and FileOutputStream object

2. Make an ObjectOutputStream
```sh 
ObjectOutputStream os = new ObjectOutputStream(fileStream);
```
ObjectOutputStream lets you write objects but it cannot directly connect to a file it needs a helper which is called chaining.

3. Write the object
```sh 
os.writeObject(characterOne); //serializes the obects referenced by character one, character two, and character three, and writes them to the file "MyGame.ser"
os.writeObject(characterTwo);
os.writeObject(characterThree);
```
4. Close the ObjectOutputStream
```sh 
os.close();
```
closing the stream at the top closes teh ones underneath the files will close automatically.

## When objects get serialized:
1. Object on the heap - Objects on the head have state,the value of the ojects instance variables. These values make one instance of a class different from 
another instance of the same class.

2. Object serialized - Serialized objects **save the values of the instance variables**, so that an identical instance (object) can be brought back to life on the heap.

When an object is serialized, all the objects it refers to from instance variables are also serialized. And all the objects those objects refer to are serialized. And all the objects those objects refer to are serialized which happens automatically.

### Mark an instance variable as transient if it cannot be saved
```sh
import java.net.*;
class Chat implements Serializable {
   transient String currentID;
String userName;
//more code
}
```
- Transient says do not save this variable during serialization just skip it.
- userName variable wil be svaead as part of the object's state during serialization.

### Deserialization: Restoring an object:
1. Make a FileInputStream
```sh
FileInputStream fileStream = new FileInputStream("MyGame.ser");
```
- Make sure MyGame.ser exists
- Make a FileInputStream object
2. Make an ObjectInputStream
```sh
ObjectInputStream os = new ObjectInputStream(fileStream);
```
- lets you read objects but cannot directly connect to a file, needs a change to connect streaming which is the FileInputStream
3. Read the objects
```sh 
Object one = os.readObject();
Object two = os.readObject();
Object three = os.readObject();
```
- Each time you say readObject(), you get the next object in the stream, you will read them back  in the ame order in which they were written 
- will get an error exception if you try to read more objects than you wrote
4. Cast the objects
```sh
GameCharacter elf = (GameCharacter) one;
GameCharacter troll = (GameCharacter) two;
GameCharacter magician = (GameCharacter) three;
```
- return value of readObject() is type Object like ArrayList, cast it back to the type you know it is
5. Close the ObjectInputStream
```sh 
os.close();
```
- Closing the stream at the top closes the ones underneath, files will close automatically

## What happens during deserialization?

1. The object is read from the stream.
2. The JVM determines the objects class type.
3. The JVM attempts to find and load the object’s class. If the JVM can’t find and/or load the class, the JVM throws an exception and the deserialization fails.
4. A new object is given space on the heap, but the serialized object’s constructor does NOT run.
5. If the object has a non-serializable class somewhere up its inheritance tree, the constructor for that non-serializable class will run along with any constructors above that (even if they’re serializable). 
- Once the constructor chaining begins, you can’t stop it, which means all superclasses, beginning with the first non-serializable one, will reinitialize their state. 
6. The object’s instance variables are given the values from the serialized state. 
- Transient variables are given a value of null for object references and defaults (0, false, etc.) for primitives.

## Writing a String to a Text File
- Saving objects through serialization is the easiest way to save and restore data between runnings of Java program.
- Writing text data is similar to writing an object, except when you write a String instead of an object, and you ues a FileWriter instead of a FileOutputStream.

**To write a serialized object:**
```sh 
objectOutputStream.writeObject(someObject);
```
**To write a String:**
```sh
fileWriter.write("My first String to save");
//we need the java.io package for FileWriter
import java.io.*; 

class WriteAFile {
  public static void main (String[] args) {

    try {
	FileWriter writer = new FileWriter("Foo.txt);
	writer.write("hello foo!");
	write.close();
    } catch(IOException ex) {
	 ex.printStackTrace();
    }
  }
}
```     
## Quiz Card Builder (code outline):
```sh
public class QuizCardBuilder {
   public void go() {
 //build and display gui, including making and registering event listners

public calss NextCardListener implements ActionListener {
  public void actionPerformed(ActionEvent ev) {
  //add the current card to the list and clear the text areas
 //triggered when user hits 'Next Card' button 
 }

private class SaveMenuListener implements ActionListener {
  public void actionPerformed(ActionEvent ev) {
  //bring up a file dialog box
 //let the user name and save the set
 }
}

private class NewMenuListener implements ActionListener {
  public void actionPerformed(ActionEvent ev) {
   //clear out the card list, and clear out the text areas
 }
}

private void saveFile(File file) {
//iterate throough the list of cards, and write each one to a text file
// in a parseable way with clear seperarations between parts
 }
}
```
### The java.io.File class
**Some things you can do with a File object:**
1. Make a File object representing an existing file
```sh
File f = new File("MyCode.txt");
```
2. Make a new directory
```sh 
File dir = new File("Chapter14")
dir.mkdir();
```
3. List the contents of a directory 
```sh
if (dir.isDirectory()) {
  String[] dirContents = dir.list();
  for (int i = 0; i < dirContents.lenght; i++) {
     System.out.println(dirContents[i]);
  }
}
```
4. Get the absolute path of a file or directory
```sh
System.out.println(dir.getAbsolutePath());
```
5. Delete a file or directory (returns true if successful)
```sh
boolean isDeleted = f.delete();
```

### The beauty of buffers
If there were no buffers, it would be like shopping without a cart. You’d have to carry each thing out to your car, one soup can or toilet paper roll at a time.
```sh
BufferedWriter writer = new BufferedWriter(new FileWriter(aFile));
```
- Only when the buffer is full will the FileWriter actually be told to write to the file on disk.

## Quiz Card Player(Code Outline)

public class QuizCardPlayer {
public void go() {
 //build and display gui
}
class NextCardListener implements ActionListener {
public voi actionPerformed(ActionEvent ev) {
//if this is a question, show the answer, otherwise show next question
// set a flag for wheter we're viewing a question or answer
  }
}

class OpenMenuListener implement ActionListener {
public void actionPerformed(ACtion event ev) {
//bring up a file dialog box
  }
}

private void loadFile(File file) {
}

private void makeCard(String lineToParse) {
 } 
}

### Parsing with String split()
**String split() lets you break a String into pieces.**

- In the QuizCardPlayer app this is what a single line looks like when it is read from the file 
```sh 
String toTest = "What is blue + yellow?/green";
```
- The split() method takes / and uses it to break apart the string into two pieces.
```sh
String[] result = toTest.split("/");
for (String token:result) {
```
- loop through the array and print each token. there are only two tokens in this example, "what is blue + yellow" and "green"
- 
    System.out.println(token);
}
```