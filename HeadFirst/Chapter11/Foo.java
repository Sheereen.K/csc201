public class Foo 
{
  public void go()
  {
    Laundry laundry = new Laundry();
   
    try {  //starting the exception try block
        laundry.doLaundry(); 
    //if doLaundry() throws a PantsException, it lands in the PantsException catch block 
      } catch (PantsException pex) { 
	//recovery code
      } catch(lingerieException lex) {
    } //end of catch
  } end of go()
}end of foo
