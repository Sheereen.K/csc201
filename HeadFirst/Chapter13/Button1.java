//add a button to the east region:
import javax.swing.*;
import java.awt.*;

public class Button1
 {
   public static void main (String[] args) 
    {
      Button1 gui = new Button1();
      gui.go();
    }

   public void go() 
    { 
      JFrame frame = new JFrame();
      JButton button = new JButton("Click this!");
      Font bigFont = new Font("serif", Font.BOLD, 28);
//a bigger font will force the frame to make more space for the button's height
      button.setFont(bigFont);
      frame.getContentPane().add(BorderLayout.NORTH, button); 
//specify the region
      frame.setSize(200, 200);
      frame.setVisible(true);
    }
 }
