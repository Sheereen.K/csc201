//FlowLayout cares about the flow in the components: left to right, top to bottom
import javax.swing.*;
import java.awt.*;

public class Panel1 
  {
    public static void main (String[] args) 
     {
       Panel1 gui = new Panel1();
       gui.go();
//panel doesn't have anything in it so it doesn't ask for much width in the east
     }  
   public void go() 
     {
       JFrame frame = new JFrame();      
       JPanel panel = new JPanel();
       panel.setBackground(Color.darkGray);
  
       panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
//change the layout manager to be a new instance of BoxLayout
//The BoxLayout constructor needs to know the component its laying out, which is why we use vertical stack
       
       JButton button = new JButton("shock me");
       JButton buttonTwo = new JButton("bliss");
    //adding button two 
       panel.add(button);
       panel.add(buttonTwo);
//adding another button
       frame.getContentPane().add(BorderLayout.EAST, panel);
//add the button to the panel and add the panel to the frame
       frame.getContentPane().add(BorderLayout.EAST, panel);
       frame.setSize(250,200);
       frame.setVisible(true);
     }
}
