## The Big Three layout managers: Border, flow, and box:
#### BorderLayout
- Divides a background component into five regions. You can add only one component per region to a backgroud controlled by a BorderLayout manager. **BorderLayout is the default layout manager for a frame.**

#### FlowLayout
- Acts kind of like a word procesor, except with components instead of words. Each component is the size it wants to be, laid out from left to right. **FlowLayout is the default layout manager for a panel.**

#### BoxLayout
- Has its own size, can stack the components vertically, you can **force** the components to start a new line.

## Changing between different regions:
- If going in the **east** or **west**, button object gets preferred width, but the height is up to the layout manager.
- If going in the **north** or **south**, it is just the opposite, recieve preferred height, but not width.
- The center region gets whatever's left

### Playing with Swing Components

#### Constructors
```sh 
JTextField field = new JTextField(20);
JTextField field = new JTextField("Your name");
```
#### How to use it
1) Get text out of it 
```sh 
System.out.println(field.getText());
```
2) Put text in it
```sh 
field.setText("whatever");
field.setText(""); //This clears the field 
```
3) Get an ActionEvent when the user presses return or enter
```sh
field.addActionListener(myActionListener;)
```
4) Select/Highlight the text in the field 
```sh 
field.selectAll();
```
5) Put the cursor back in the field so the user can start typing
```sh 
field.requestFocus();
```
### JCheckBox
#### Constructor
```sh 
JCheckBox check = new JCheckBox("Goes to 11);
```
#### How to use it 
1. Listen for an item event(when it's selected or deselected)
```sh 
check.addItemListener(this);
```
2. Handle the event and find uot whether or not it's selected
```sh
public void itemStateChanged(ItemEvent ev) {
   String onOrOff = "off";
   if (check.isSelected()) onOrOff = "on";
   System.out.println("Check box is " + onOrOff);
 }

3. Select of deselect it in code
```sh
check.setSelected(true);
check.setSelected(false);
```
### JList
#### Constructor
- JList contructor takes an array of any object type
```sh
String [] listEntries = {"alpha", "beta", "gamma", "delta", "epsilon", "zeta "};
list = new JList
```
#### How to use it 
1. Make it have a vertical scrollbar
```sh
JScrollPane scroller = new JScrollPane(list);
scroller.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
scroller.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
panel.add(scroller);
```
2. Set the number of lines to show before scrolling
```sh
list.setVisibleRowCount(4);
```
3. Restrict the user to selecting only ONE thing at a time
```sh
 list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
```
4. Register for list selection events
```sh 
list.addListSelectionListener(this);
``` 
5. Handle events (find out which thing in the list was selected)
```sh
public void valueChanged(ListSelectionEvent lse) {
       if( !lse.getValueIsAdjusting()) { //you will get the event TWICE if you dont put in this if test
         String selection = (String) list.getSelectedValue(); //get SelectedValue() actually returns an Object. A list isnt limited to only String objects
         System.out.println(selection);
 }
   }
```

