import java.awt.*;
import javax.swing.*;
import javax.sound.midi.*;
import java.util.*;
import java.awt.event.*;

public class BeatBox 

 {
    JPanel mainPanel;
    ArrayList<JCheckBox> checkboxList; //store the checkboxes in an ArrayList
    Sequencer sequencer;
    Sequence sequence;
    Track track;
    JFrame theFrame;
//The names of the instruments, as a String array, for building th GUI labels
    String[] instrumentNames = {"Bass Drum", "Closed Hi-Hat", 
       "Open Hi-Hat", "Acoustic Snare", "Crash Cymbal", "Hand Clap", 
       "High Tom", "Hi Bongo", "Maracas", "Whistle", "Low Conga", 
       "Cowbell", "Vibraslap", "Low-mid Tom", "High Agogo", 
       "Open Hi Conga"};
    int[] instruments = {35,42,46,38,49,39,50,60,70,72,64,56,58,47,67,63};
// The numbers are the actual drum keys, the drum channel is like a piano except each key on the piano is a different drum. 
    public static void main (String[] args) {
        new BeatBox().buildGUI();
    }
    public void buildGUI() {
        theFrame = new JFrame("Cyber BeatBox");
        theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        BorderLayout layout = new BorderLayout();
        JPanel background = new JPanel(layout);
        background.setBorder(BorderFactory.createEmptyBorder(10,10,10,10));
//An 'empty border' gives us a margin between the edges of the panel and where the componentts are placed for aesthetics
 
        checkboxList = new ArrayList<JCheckBox>();
        Box buttonBox = new Box(BoxLayout.Y_AXIS);

        JButton start = new JButton("Start");
        start.addActionListener(new MyStartListener());
        buttonBox.add(start); 
        
        JButton stop = new JButton("Stop");
        stop.addActionListener(new MyStopListener());
        buttonBox.add(stop);

        JButton upTempo = new JButton("Tempo Up");
        upTempo.addActionListener(new MyUpTempoListener());
        buttonBox.add(upTempo);

        JButton downTempo = new JButton("Tempo Down");
//more GUI code 
        downTempo.addActionListener(new MyDownTempoListener());
        buttonBox.add(downTempo);

        Box nameBox = new Box(BoxLayout.Y_AXIS);
        for (int i = 0; i < 16; i++) {
           nameBox.add(new Label(instrumentNames[i]));
        }

        background.add(BorderLayout.EAST, buttonBox);
        background.add(BorderLayout.WEST, nameBox);

        theFrame.getContentPane().add(background);

        GridLayout grid = new GridLayout(16,16);
        grid.setVgap(1);
        grid.setHgap(2);
        mainPanel = new JPanel(grid);
        background.add(BorderLayout.CENTER, mainPanel);

//Make the checkboxes, set them to 'false'so they aren't checled and add them to the ArrayList and the GUI panel
        for (int i = 0; i < 256; i++) {                    
            JCheckBox c = new JCheckBox();
            c.setSelected(false);
            checkboxList.add(c);
            mainPanel.add(c);            
        } // end loop
        
        setUpMidi();

        theFrame.setBounds(50,50,300,300);
        theFrame.pack();
        theFrame.setVisible(true);
    } // close method

    public void setUpMidi() {
      try {
        sequencer = MidiSystem.getSequencer();
        sequencer.open();
        sequence = new Sequence(Sequence.PPQ,4);
        track = sequence.createTrack();
        sequencer.setTempoInBPM(120);

      } catch(Exception e) {e.printStackTrace();}
    } // close method
// The MIDI set up for getting Sequencer, the Sequence, and the Track

//make a 1b-element array to hold the values for one instrument across all 1b beats. if the instrument is supposed to play on that beat, the value at that element will be the key. If not, put a zero
    public void buildTrackAndStart() {
      int[] trackList = null;

//get rid of the old track and make a fresh one
      sequence.deleteTrack(track);
      track = sequence.createTrack();

//do this for each of the 1b ROWS ex.Bass, Congo, etc.
        for (int i = 0; i < 16; i++) {
          trackList = new int[16];
//Set the 'key', that shows which instrument this is, the instrument array holds the actual MIDI number for each instrument
          int key = instruments[i];
//do this for each of the BEATS for this row
          for (int j = 0; j < 16; j++ ) {

//checkbox selected, if yes, put the key value in this slot in the array(the slot that represents this beat.) if no, the instrument is not supposed to play, so set to zero 
              JCheckBox jc = (JCheckBox) checkboxList.get(j + (16*i));
              if ( jc.isSelected()) {
                 trackList[j] = key;
              } else {
                 trackList[j] = 0;
              }                    
           } // close inner loop

//for this instrument, and for 1b beats, make events and add them to the track
           makeTracks(trackList);
           track.add(makeEvent(176,1,127,0,16));  
       } // close outer

//always want to make sure that there IS an event at 1b, or it won't go to full 1b beats before it starts over
       track.add(makeEvent(192,9,1,0,15));      
       try {
//loop continuously lets you specify the number of loop iterations, or continuous looping
          sequencer.setSequence(sequence); 
           sequencer.setLoopCount(sequencer.LOOP_CONTINUOUSLY);                   
//play the beatbox           
           sequencer.start();
           sequencer.setTempoInBPM(120);
       } catch(Exception e) {e.printStackTrace();}
    } // close buildTrackAndStart method

//first of the inner classes, listeners for the buttons.
    public class MyStartListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            buildTrackAndStart();
        }
    } // close inner class

    public class MyStopListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
            sequencer.stop();
        }
    } // close inner class
   
    public class MyUpTempoListener implements ActionListener {
        public void actionPerformed(ActionEvent a) {
          float tempoFactor = sequencer.getTempoFactor(); 
            sequencer.setTempoFactor((float)(tempoFactor * 1.03));
        }
     } // close inner class
//the other inner class listeners for the buttons
     public class MyDownTempoListener implements ActionListener {
         public void actionPerformed(ActionEvent a) {
           float tempoFactor = sequencer.getTempoFactor();
//the Tempo Factor scales the sequencer's tempo by the factor provided, default is 1.0, so adjust +/-3% per click
            sequencer.setTempoFactor((float)(tempoFactor * .97));
        }
    } // close inner class

//this makes events for one instrument at a time, for all 1b beats. 
     public void makeTracks(int[] list) {        
       for (int i = 0; i < 16; i++) {
          int key = list[i];
//make the NOTE ON and NOTE OFF events, and add them to the Track
          if (key != 0) {
             track.add(makeEvent(144,9,key, 100, i));
             track.add(makeEvent(128,9,key, 100, i+1));
          }
       }
    }
//this is the utility method from the last chapter 
    public  MidiEvent makeEvent(int comd, int chan, int one, int two, int tick) {
        MidiEvent event = null;
        try {
            ShortMessage a = new ShortMessage();
            a.setMessage(comd, chan, one, two);
            event = new MidiEvent(a, tick);
        } catch(Exception e) {e.printStackTrace(); }
        return event;
    }
} // close class

public class MySendListener implements ActionListener 
 {
//it all happens whent he user clicks the button and the action event fires
   public void actionPerformed(ActionEvent a)
     {
//it all happens when the user clicks the button and the ActionEvent fires
        boolean[] checkboxState = new boolean[256]; 
//make a boolean array to hold the state of each checkbox

        for (int i = 0; i < 256; i++) 
           {
	     JCheckBox check = (JCheckBox) checkboxList.get(i);
	     if (check.isSelected()) 
              { 
		  checkboxState[i] = true;
//walk through the checkboxList and get the state of each one and add it to the boolean array
    	     }
	   }

	   try {
              FileOutputSteam fileStream = new FileOutputStream(new File("Checkbox.ser"));
	      ObjectOutputStream os = new ObjectOutputStream(fileStream);
	      os.writeObject(checkboxState);
	    }
	   catch(Exception ex) {
		   ex.printStackTrace();
	    }

      }
 }


public class MyReadInListener implements ActionListener 
   {
      public void actionPerformed(ActionEvent a) 
       {
	 boolean[] checkboxState = null;
	 try {
 	     FileInputStream fileIn = new FilesInputStream(new File("Checkbox.ser"));
	     ObjectInputStream is = new ObjectInputStream(fileIn);
	     checkboxState = (boolean[]) is.readObject();
//read the single object in the file and cast it back to a boolean array 
	    } catch(Exception ex) {ex.printStackTrace();}


	   for (int i = 0; i < 256; i++) {
		   JCheckBox check = (JCheckBox) checkboxList.get(i);
		   if (checkboxState[i]) {
			   check.setSelected(true);
//restore the state of each of the checkboxes in the ArrayList of actual JCheckbox objects
		   }
		       else {
			   check.setSelected(false);
		       }
	     }
	   sequencer.stop();
           buildTrackAndStart();
      } //now stop whatever is currently playing and rebuild the sequence using the new state of the checkboxes in the ArrayList
   }


