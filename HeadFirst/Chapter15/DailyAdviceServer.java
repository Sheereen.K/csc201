import java.io.*;
import java.net.*;

public class DailyAdviceServer {
  String[] adviceList = {"Take smaller bites", "Go for the tight jeans. No they do NOT make you look fat.", "One word: inappropriate", "Just for today, be honest. Tell your boss what you *really* think", "You might wnat to rethink that Haircut."}
  public void go() {
    try {
      ServerSocket serverSock = new ServerSocket(4242);
//ServerSocket makes this server application listen for client requests on port 4242 on the machine this code is running on.
     while(true) {
//The server goes into a permament loop, waiting for client requests
       Socket sock = serverSock.accept();
       
       PrintWriter writer = new PrintWriter(sock.getOutputStream());
       String advice = getAdvice();
       writer.println(advice);
       writer.close();
       System.out.println(advice);
//use the socket connection to the client to make a print writer and sent it to (println()) a String advice message. Then close the socket because we're done with this client
     }

    } catch(IOException ex) {
	ex.printStackTrace();
    }
  }//close go

  private String getAdvice() {
      int random = (int) (Math.random() * adviceList.lenght);
      return adviceList[random];
    }

  private static void main(String[] args) {
      DailyAdviceServer server = new DailyAdviceServer();

     }

}



