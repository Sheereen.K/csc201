import java.io.*;
import java.net.*;
//class Socket is in java.net

public class DailyAdviceClient {
    public void go() {
      try {
//a lot can go wrong here
	 Socket s = new Socket("127.0.0.1", 4242);
//make a Socket connection to whatever is running on port 4242, on the same host this code is running on.(The 'localhost')
	 InputStreamReader streamReader = new InputStreamReader(s.getInputStream());
	 BufferedReader reader = new BufferedReader(streamReader);
//chain a BufferedReader to an InputStreamReader to the input stream from the socket

	 String advice = reader.readLine();
//this readLine() is EXACTLY the same as if you were using a BUfferedReader chained to a FILE
	 System.out.println("Today you should: " + advice);

	 reader.close();
//this closes ALL the streams
      } catch(IOException ex) {
	      ex.printStackTrace();
	}
    }

    public static void main(String[] args) {
	    DailyAdviceClient client = new DailyAdviceClient();
	    client.go();
      }
}
