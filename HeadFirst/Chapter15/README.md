## Real Time Beat Box Chat
**Chat Program Overview: How it works**
1. Client connects tothe server 
2. The server makes a connection and adds to the list of participants
3. Another client connects
4. Client A sends a message to the chat
5. The server distributed the message to ALL participants 

### Connecting, sending, and receiving 

1. How to establish the initial connection between the client and server
2. How to send messages to the server
3. How to receive messages from the server

**1. Connect**
Client connects to the server by establishing a Socket connection.
**2. Send**
Client sends a message to the server
**3. Receive**
Client gets a message from the server 

### Make a network socket connection 
- To make a Socket connection, you need to know two things about the server: who it is, and which port it's running on. 
- A socket connection means the two machines have information about each other, including network location(IP address) and TCP port.

**A TCP port is just a number. A 16-bit number that identifies a specific program on the server.**
- The TCP port numbers from 0 to 1023 are reserved for well-known services. Don't use them for your own server programs
- The chat server we're writing uses port 5000. We just picked a number between 1024 and 65535

### To **read** data from a Socket, use a BufferedReader 
1. Make a Socket connection to the server
```sh 
socket chatSocket = new Socket("127.0.0.1", 5000); 
```
2. Make an InputStreamReader chained to the Socket's low-level(connection) input stream
```sh
InputStreamReader stream = new InputStreamReader(chatSocket.getInputStream());
```
3. Make a BufferedReader and read
```sh 
BufferedReader reader = new BufferedReader(stream);
String message = reader.readLine();
```
### To **write** data to a Socket, use a PrintWriter
1. Make a Socket connection to the server
```sh
Socket chatSocket = new Socket("127.0.0.1", 5000);
```
2. Make a PrintWriter chained to the Socket's low-level (connection) output stream
```sh 
PrintWriter writer = new PrintWriter(chatSocket.getOutputStream());
```
3. Write(print) something
```sh
writer.println("message to send");
writer.print("another message");
```

### The DailyAdviceClient
1. Connect
- client connects to the server and gets an input stream from it 
2. Read
- Client reads a message from the server
3. Read 
- Client reads a message from the server

### Writing a simple server
1. Server application makes a server socket on a specific port
```sh 
ServerSocket serverSock = new ServerSocket(4242);
```
- this starts the server applicaiton listening for client requests coming in for port 4242.
2. Client makes a Socket connection to the server application
```sh
Socket sock = new Socket("190.165.1.103", 4242);
```
- Client knows the IP address and port number 
3. Server makes a new Socket to communicate with this client
```sh 
Socket sock = serverSock.accept();
```
###Java has multiple threads but only one Thread class
- A thread is a seperate 'thread of excecution'. In other words, a seperate call stack.
- A Thread is a Java class that represents a thread.
- To make a thread, make a Thread.

###Waht does it mean to have more than one call stack?
- you get the ***appearance*** of having multiple things happen at the same time.
- it can appear that you are doing several things simultaneously.
1. the JVM calls the main() method
2. main() starts a new thread. the main thread is temporarily frozen whle the new thread starts running.
3. The JVM switches between the new thread (user thread A) and the original main thread, until both threads are complete.

## How to launch a new thread:
1. Make a Runnable object(the thread's job)
```sh
Runnable threadJob = new MyRunnable();
```
2. Make a Thread object (the worker) and give it Runnable (the job)
```sh 
Thread myThread = new Thread(threadJob);
```
3. Start the Thread
```sh
myThread.start();
```

#### Every Thread needs a job to do. A method to put on the new thread stack
- Runnable is to a Thread what a job is to a worker. A Runnable is the job a thread is supposed to run.
- A Runnable holds the method that goes on the bottom of the new thread's stack: run()
- When you pass Runnable to a Thread constructor, you are really just giving the Thread a way to get to a run() method. You are giving the Thread its job to do.

### To make a job for your thread, implement the Runnable interface

public class MyRunnable implements Runnable {
//Runnable is in the java.lang package, so you don't need to import it.

public void run() {
   go();
}
//runnable has only one method to implement: public void run(). This is where you put the JOB the thread is supposed to run. This is the method that goes at the bottom of the new stack.

public void go() {
   doMore();
}

public void doMore() {
   System.out.println("top o' the stack");
 }
}

class ThreadTester {
   public static void main (String[] args) {
     Runnable threadJob = new MyRunnable();
     Thread myThread = new Thread(threadJob);

1. myThread .start();
   System.out.println("back in main");
  }
}

#### The three states of a new thread
- **New** 
- Thread t = new Thread(r);
- **Runnable**
- t.start();
- **Running**
- OR **Blocked**

### The Thread Scheduler 
- The thread scheduler makes all the decisions about who runs and who doesn't. He usually makes the threads take turns
- No guarentee about that
- Might let one thread run and the other thread starve

###Putting a thread to sleep
- Put your thread to sleep if you want to be sure that other threads get a chance to run.
- When the thread wakes up, it always goes back to the runnable state and waits for the thread scheduler to choose it to run again.
```sh
try {
  Thread.sleep(2000);
 } catch(InterruptedException ex) {
    ex.printStackTrace();
}
```
- Your thread will probably never be inturrupted from sleep.


