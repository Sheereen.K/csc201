public abstract class Animal 
{ 
 public void makeNoise()
  {
   System.out.println("Animal makes noise.");
  }
 public void eat()
  { 
   System.out.println("Animal gets hungry.");
  }
 public void sleep()
  { 
   System.out.println("Animal sleeps.");
  }
 public void roam()
  {
   System.out.println("Animal roams.");
  } 

}
