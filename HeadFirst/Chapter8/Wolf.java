public class Wolf extends Canine
{ 
  public void makeNoise()
  {
  System.out.println("Wolf is howling");
  }

  public void eat()
  {
  System.out.println("Wolf eats meat");
  }
}
